 <?php
    session_start(); // Demarrage de la session
    
    // *** VERIFICATION CONNEXION/SALARIE ***
    if(!isset($_SESSION['profil']) || $_SESSION['profil'] != 'salarie'){
        header('Location: index.php');
    }
    
    // *** VERIFICATION DUREE DE LA SESSION ***
    if (!isset($_SESSION['timeout_idle'])) {
        $_SESSION['timeout_idle'] = time() + 2*24*60*60; // Si existe pas on créer pour 48h
    } 
    else {
        if ($_SESSION['timeout_idle'] < time()) {   // Si temps ecoulé => deconnexion
            header('Location: deconnexion.php');
        } 
        else {
            $_SESSION['timeout_idle'] = time() + 2*24*60*60; // Sinon on ralonge
        }
    }
    
    // *** CONNEXION A LA BDD ***
    $connect = mysqli_connect("127.0.0.1","root","","base_projet");
    try{
        $bdd = new PDO('mysql:host=localhost;dbname=base_projet;charset=utf8', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
    }
    catch(Exception $e){
        die('Erreur :' .$e->getMessage());
    }

    // *** RECUPERATION DES INFOS ***
    $verif = $bdd->query('SELECT * FROM salarie WHERE id_salarie = \'' . $_SESSION['id'] . '\'AND profil = \'' . $_SESSION['profil'] . '\'');
    while($var = $verif->fetch()){
        $prenom = $var['prenom'];
        $nom = $var['nom'];
        $mail = $var['mail'];
    }
    $verif->closeCursor();

    // Après clic sur le bouton envoyer on récupère les données envoyées par la méthode post
    if(isset($prenom) && isset($nom) && isset($mail) && isset($_POST["com"]))
    {
        //reception commentaire de contact
        $nom = mysqli_real_escape_string($connect,$nom);
        $prenom = mysqli_real_escape_string($connect,$prenom);
        $mail = mysqli_real_escape_string($connect,$mail);
        $com = mysqli_real_escape_string($connect,$_POST["com"]);
        if(preg_match("#^[a-zA-Z]{2,}.[a-zA-Z]{2,}[@](esme).fr$#", $mail))
        {
            $sql = "INSERT INTO `commentaire`(`com`, `mail`) VALUES ('$com','$mail')";

            //exécuter la requête d'insertion
            $var =5;
            $var = mysqli_query($connect, $sql) or die(mysqli_error($connect));
            
        }
        else if(preg_match("#^[a-zA-Z]{2,}.[a-zA-Z]{2,}[@](esme).fr$#", $mail)) 
            echo "<p style ='color:red'>le salarié n'est pas valide </p>";

        //header("Location:gestionSalarie.php?message=$message");
    }
    
    $id_salarie = $_SESSION['id'];
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Gestion Des Congés</title>
        <img src="Logo_esme.jpg" height="200" width="320" alt> 
    <head>
    <body>
        <nav>
            <a href="gestionConges.php">Accueil</a>
            <a href="gestionProfil.php">Gestion Profil</a>
            <a href="deconnexion.php">Déconnexion</a>
	</nav>
        <form name="exe" action="contact.php?id=<?php echo $_GET["mail"];?>" method="post">
            <fieldset>
                <legend>Contact</legend>

                <input type="hidden" id_salarie="id" name="id_salarie" value="<?php if(isset($id_salarie)) { echo $id_salarie; } ?>"><br/>
                <label for="nom">Nom</label>
                <input type="text" id="nom" name="nom" required value="<?php if(isset($nom)) { echo $nom; } ?>"><br/>
                <label for="prenom">Prénom</label>
                <input type="text" id="prenom" name="prenom" required value="<?php if(isset($prenom)) { echo $prenom; } ?>"><br/>
                <label for="mail">Mail</label>
                <input type="email" id="mail" name="mail" required value="<?php if(isset($mail)) { echo $mail; } ?>"><br/>
            </fieldset>

            <fieldset>
                <legend>Message à envoyer</legend>
                <p>
                    <textarea name="com" id="com" cols="40" rows="4" required value="<?php if(isset($com)) { echo $com; } ?>"></textarea>
                    <!--<textarea name="com" id="com" cols="40" rows="4"></textarea>-->
                </p>
            </fieldset>

            <input type="submit" value="Envoyer"/>
        </form>
        
        <?php
            if ($var) {
                 $message= "Le commentaire a été ajouté  avec succès !";
                 echo "Le commentaire a été ajouté  avec succès </br></br>";
            }
            else if ($var = 5) {echo "";}
            else {
                $message= "Erreur d'insertion ";    
                echo "Erreur d'insertion </br>";
            }
        ?>
        
    </body>
        Adresse : 38 rue Molière 94200 Ivry-sur-Seine <br>
        Télephone : 01 56 20 62 00
    </footer>
</html>