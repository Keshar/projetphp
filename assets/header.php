<?php
    session_start(); // Demarrage de la session
    
    // Permet d'eviter qu'un salarié puisse aller ici
    if(!isset($_SESSION['profil']) || $_SESSION['profil'] != 'salarie'){
        header('Location: index.php');
    }
    
    // Vérification de la duree de la session
    if (!isset($_SESSION['timeout_idle'])) {
        $_SESSION['timeout_idle'] = time() + 2*24*60*60; // Si existe pas on créer pour 48h
    } 
    else {
        if ($_SESSION['timeout_idle'] < time()) {   // Si temps ecoulé => deconnexion
            header('Location: deconnexion.php');
        } 
        else {
            $_SESSION['timeout_idle'] = time() + 2*24*60*60; // Sinon on ralonge
        }
    }
?>
<!DOCTYPE html>
<html>
    <meta charset="UTF-8">
    <title>Gestion Des Congés</title>
    <img src="Logo_esme.jpg" height="200" width="320" alt>
    <link rel="stylesheet" href="../index.css">
    <head>
        </br>
        <nav>
        <?php if($_SESSION['profil'] === 'admin'): ?>
            <a href='gestionConges.php'>Accueil</a>
            <a href='gestionSalarie.php'>Gestion Salarié</a>
            <a href='consultationCommentaire.php'>Consultation des Commentaires</a>
        <?php else: ?>
            <a href="gestionConges.php">Accueil</a>
            <a href="gestionProfil.php">Gestion du profil</a>
            <a href="contact.php">Contact</a>
        <?php endif; ?>
            <a href="deconnexion.php">Déconnexion</a>
            </br></br></br>
        </nav>
    </head>
</html>