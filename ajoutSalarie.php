<?php

/***********************************/
/*     Génère un mot de passe      */
/***********************************/
// $size : longueur du mot passe voulue
function mdp_aleatoire($size)
{
    $password ="";
    // Initialisation des caractères utilisables
    $characters = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");

    for($i=0;$i<$size;$i++)
    {
        $password .= ($i%2) ? strtoupper($characters[array_rand($characters)]) : $characters[array_rand($characters)];
    }
		
    return $password;
}

?>


<html>
    <head>
        <meta charset="UTF-8">
        <title>Gestion Des Congés</title>
        <img src="Logo_esme.jpg" height="200" width="270" > <!-- attention j'ai niquée les dimensions-->
    </head>
    <body>
        <nav>
            <a href="gestionConges.php">Accueil</a>
            <a href="gestionSalarie.php">Gestion Salariés</a>
            <a href="deconnexion.php">Déconnexion</a>
	</nav>
        
        <?php
        $connect = mysqli_connect("127.0.0.1","root","","base_projet");
        
        if (!$connect) {
            echo"echec de connection";
        }
        
        //Après appel de la page on récupéré l'id du livre en question
        if(isset($_POST["nom"]) && isset($_POST["prenom"]) && isset($_POST["mail"]) && isset($_POST["telephone"]) /*&& isset($_POST["fonction"])*/&& isset($_POST["type_contrat"]) && isset($_POST["date_embauche"]) && isset($_POST["nb_conge_rtt"]) && isset($_POST["nb_conge_paye"]) )
        {
            //La fonction mysqli_real_escape_string est utilisée pour créer une chaîne 
            //SQL valide qui pourra être utilisée dans une requête SQL. La chaîne de caractères 
            //escapestr est encodée en une chaîne SQL échappée,en tenant compte du jeu de caractères courant de la connexion. 
            // elle échappe les caractères spéciaux (ajoute antislash devant l'apostrophe, etc.	
            $nom = mysqli_real_escape_string($connect,$_POST["nom"]);
            $prenom = mysqli_real_escape_string($connect,$_POST["prenom"]);
            $mail = mysqli_real_escape_string($connect,$_POST["mail"]);
            $telephone = mysqli_real_escape_string($connect,$_POST["telephone"]);
            $fonction = mysqli_real_escape_string($connect,$_POST["fonction"]);
            $type_contrat = mysqli_real_escape_string($connect, $_POST["type_contrat"]);
            $date_embauche = mysqli_real_escape_string($connect, $_POST["date_embauche"]);
            $nb_conge_rtt = mysqli_real_escape_string($connect,$_POST["nb_conge_rtt"]);
            $nb_conge_paye = mysqli_real_escape_string($connect,$_POST["nb_conge_paye"]);
            $mdp_salarie = mdp_aleatoire(8);
            $adresse = mysqli_real_escape_string($connect,$_POST["adresse"]);
            $nationalite = mysqli_real_escape_string($connect,$_POST["nationalite"]);
            $sexe = mysqli_real_escape_string($connect,$_POST["sexe"]);
            $situation = mysqli_real_escape_string($connect,$_POST["situation"]);
            $age = mysqli_real_escape_string($connect,$_POST["age"]);
            
            if(preg_match("#^[0][0-9]{7}$#", $telephone) && preg_match("#^[a-zA-Z]{2,}$#", $nom) && preg_match("#^[a-zA-Z]{2,}$#", $prenom)&& preg_match("#^[a-zA-Z]{2,}.[a-zA-Z]{2,}[@](esme).fr$#", $mail) /*&& preg_match("#^(Enseignant|Personnel administratif|enseignant|personnel administratif)$#", $fonction) && preg_match("#^(CDD|CDI)$#", $type_contrat)*/ && preg_match("#^[0-3][0-9]-[0-1][0-9]-[1-2](0|9)[0-9]{2}$#", $date_embauche) && preg_match("#^[0-9]{1,2}$#", $nb_conge_paye) && preg_match("#^[0-9]{1,2}$#", $nb_conge_rtt)){
                if(/*preg_match("#^[0-9]{5}, [a-zA-Z]|é|è){2,}, [0-9]{1,3} (rue|avenue|boulevard|place|quai) *$#", $adresse) ||*/ $adresse =="")
                {
                        if(preg_match("#^([a-zA-Z]|é|è){2,}$#", $nationalite) || $nationalite =="")
                        {
                            if(preg_match("#^(H|F)$#", $sexe) || $sexe=="")
                            {
                                //if(preg_match("#^(en couple|marié|mariée|voeuf|voeuve|célibataire)$#", $situation) || $situation=="")
                                //{
                                    
                                    $sql = "INSERT INTO salarie (nom, prenom, mail, telephone, fonction, type_contrat, date_embauche,nb_conge_rtt, nb_conge_paye, mdp_salarie, adresse, nationalite, sexe, situation, age)
                                    VALUES ('$nom', '$prenom', '$mail', '$telephone', '$fonction', '$type_contrat', '$date_embauche', '$nb_conge_rtt', '$nb_conge_paye', '$mdp_salarie', '$adresse', '$nationalite', '$sexe', '$situation', '$age')";

                                    //exécuter la requête d'insertion
                                    $var = mysqli_query($connect, $sql) or die(mysqli_error($connect));
                                    if ($var) 
                                    {
                                         $message= "Le salarié a été ajouté  avec succès";
                                         echo "Le salarié a été ajouté  avec succès";
                                    }
                                    else
                                    {
                                        $message= "Erreur d'insertion ";    
                                        echo "Erreur d'insertion";
                                    }

                                //}
                            }
                        }
               
                }
            }
            else if(!preg_match("#^[0][0-9]{7}$#", $telephone) && preg_match("#^([a-zA-Z]|é|è){2,}$#", $nom) && preg_match("#^([a-zA-Z]|é|è){2,}$#", $prenom)&& preg_match("#^[a-zA-Z]{2,}.[a-zA-Z]{2,}[@](esme).fr$#", $mail) /*&& preg_match("#^(Enseignant|Personnel administratif|enseignant|personnel administratif)$#", $fonction) && preg_match("#^(CDD|CDI)$#", $type_contrat)*/ && preg_match("#^[0-3][0-9]-[0-1][0-9]-[1-2](0|9)[0-9]{2}$#", $date_embauche) && preg_match("#^[0-9]{1,2}$#", $nb_conge_paye) && preg_match("#^[0-9]{1,2}$#", $nb_conge_rtt)) 
                echo "<p style ='color:red'>le salarié n'est pas valide </p>";
            
            header("Location:gestionSalarie.php?message=$message");
        }

        ?>
<!--  Afficher le formulaire rempli par les données du livre récupéré en haut.-->
<form name="exe" action="ajoutSalarie.php?id=<?php echo $_GET["id"];?>" method="post">
      		<fieldset>
      			<legend>Ajouter un Salarié</legend>
                        <fieldset>
                            <legend>Champs Obligatoires</legend>
                            <input type="hidden" id_salarie="id" name="id_salarie" value="<?php if(isset($id_salarie)) { echo $id_salarie; } ?>"><br/>
                            <label for="nom">Nom</label>
                            <input type="text" id="nom" name="nom" required value="<?php if(isset($nom)) { echo $nom; } ?>"><br/>
                            <label for="prenom">Prénom</label>
                            <input type="text" id="prenom" name="prenom" required value="<?php if(isset($prenom)) { echo $prenom; } ?>"><br/>
                            <label for="mail">Mail</label>
                            <input type="mail" id="mail" name="mail" required value="<?php if(isset($mail)) { echo $mail; } ?>"><br/>
                            <label for="telephone">Numéro de téléphone</label>
                            <input type="text" id="telephone" name="telephone" required value="<?php if(isset($telephone)) { echo $telephone; } ?>"><br/>
                            <label for="fonction">Fonction</label>
                            <select name="fonction" id="fonction">
                                <option value="enseignant" >Enseignant</option>
                                <option value="personnel administratif" >Personnel Administratif</option> 
                            </select>   <br/>
                            <!-- <input type="txt" id="fonction" name="fonction" required value="<?php if(isset($fonction)) { echo $fonction; } ?>"><br/> -->
                            <label for="type_contrat">Type de contrat</label>
                            <select name="type_contrat" id="type_contrat">
                                <option value="CDD" >CDD</option>
                                <option value="CDI" >CDI</option> 
                            </select>   <br/>
                            <!-- <input type="text" id="type_contrat" name="type_contrat" required value="<?php if(isset($type_contrat)) { echo $type_contrat; } ?>"><br/> -->
                            <label for="date_embauche">Date d'embauche</label>
                            <input type="date" id="date_embauche" name="date_embauche" required value="<?php if(isset($date_embauche)) { echo $date_embauche; } ?>"><br/>
                            <label for="nb_conge_rtt">Nombre de congés restant RTT</label>
                            <input type="int" id="nb_conge_rtt" name="nb_conge_rtt" required value="<?php if(isset($nb_conge_rtt)) { echo $nb_conge_rtt; } ?>"><br/>
                            <label for="nb_conge_paye">Nombre de congés restant payés</label>
                            <input type="int" id="nb_conge_paye" name="nb_conge_paye" required value="<?php if(isset($nb_conge_paye)) { echo $nb_conge_paye; } ?>"><br/>
                        </fieldset>
                        <fieldset>
                            <legend>Champs Optionnels</legend>
                            <input type="hidden" id_salarie="id" name="id_salarie" value="<?php if(isset($id_salarie)) { echo $id_salarie; } ?>"><br/>
                            <label for="adresse">Adresse</label>
                            <input type="text" id="adresse" name="adresse" value="<?php if(isset($adresse)) { echo $adresse; } ?>"><br/>
                            <label for="nationalite">Nationalité</label>
                            <input type="text" id="nationalite" name="nationalite" value="<?php if(isset($nationalite)) { echo $nationalite; } ?>"><br/>
                            <label for="sexe">Sexe</label>
                            <select name="sexe" id="sexe">
                                <option value="" >Choisir...</option> <!-- tester : <option value=NULL >Choisir...</option> -->
                                <option value="F" >Femme</option>
                                <option value="H" >Homme</option> 
                                <option value="A" >Autre</option> 
                            </select>   <br/>
                            <label for="situation">Situation</label>
                            <select name="situation" id="situation">
                                <option value="" >Choisir...</option>
                                <option value="celibataire" >Célibataire</option>
                                <option value="marie" >Marié.e</option> 
                                <option value="veuf" >Veuf.ve</option> 
                                <option value="paxe" >Paxé.e</option> 
                            </select>   <br/>
                            <label for="age">Age</label>
                            <input type="txt" id="age" name="age" value="<?php if(isset($age)) { echo $age; } ?>"><br/>
                            
                        </fieldset>
                        <input Type="submit" value="Ajouter">
      		</fieldset>
      </form>
    </body>
    <footer>
        Adresse : 38 rue Molière 94200 Ivry-sur-Seine <br>
        Télephone : 01 56 20 62 00
    </footer>
</html>