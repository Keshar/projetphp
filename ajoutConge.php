<?php
    session_start(); // Demarrage de la session
    
    // Vérification de l'existance d'une session 
    // Permet d'eviter d'aller sur une page avec son url sans sessions
    if(!isset($_SESSION['profil'])){
        header('Location: index.php');
    }
    
    // Vérification de la duree de la session
    if (!isset($_SESSION['timeout_idle'])) {
        $_SESSION['timeout_idle'] = time() + 2*24*60*60; // Si existe pas on créer pour 48h
    } 
    else {
        if ($_SESSION['timeout_idle'] < time()) {   // Si temps ecoulé => deconnexion
            header('Location: deconnexion.php');
        } 
        else {
            $_SESSION['timeout_idle'] = time() + 2*24*60*60; // Sinon on ralonge
        }
    }
    
    $connect = mysqli_connect("127.0.0.1","root","","base_projet");
    
?>

<?php
        
        if(isset($_POST["date_debut"]) && isset($_POST["date_fin"]) && isset($_POST["nb_jours"])) {
        
            $date_debut = mysqli_real_escape_string($connect,$_POST["date_debut"]);
            $date_fin = mysqli_real_escape_string($connect,$_POST["date_fin"]);
            $nb_jours = mysqli_real_escape_string($connect,$_POST["nb_jours"]);
            
            
            if(preg_match("#^(0?[1-9]|1[0-9]|2[0-4])$#", $nb_jours)) {
                                    
                                    $sql = "INSERT INTO conges (id_salarie, date_debut, date_fin, nb_jours)
                                            VALUES (" . $_SESSION['id']. ", '" .$date_debut. "', '" .$date_fin. "', '" .$nb_jours. "')";
                                    //exécuter la requête d'insertion
                                    $insert = mysqli_query($connect, $sql) or die(mysqli_error($connect));
                                    if ($insert) 
                                    {
                                         $message= "Votre demande de congé a bien été enregistrée";
                                         echo "Votre demande de congé a bien été enregistrée";
                                    }
                                    else
                                    {
                                        $message= "Erreur d'enregistrement de votre demande";    
                                        echo "Erreur d'enregistrement de votre demande";
                                    }

            }    
        }
  ?>

            
<html> 
    <meta charset="UTF-8">
    <title>Gestion Des Congés</title>
    <img src="Logo_esme.jpg" height="200" width="320" alt>
    <link rel="stylesheet" href="../index.css">
    <head>
        </br>
        <nav>
            <a href='gestionConges.php'>Accueil</a>
            <a href='consultationCommentaire.php'>Consultation des Commentaires</a>
            <a href='deconnexion.php'>Déconnexion</a>
            </br></br></br>
        </nav>
    </head>
    <body>
            <form name="exe" action="ajoutConge.php?id=<?php echo $_GET["id"];?>" method="post">
      		<fieldset>
      			<legend>Demande de congé</legend>
                        <fieldset>
                            <legend>Champs Obligatoires</legend>
                            <input type="hidden" id="id_conge" name="id_conge" value="<?php if(isset($id_conge)) { echo $id_conge; } ?>"><br/>
                            <label for="date_debut">Date de début de congé</label>
                            <input type="date" id="date_debut" name="date_debut" required value="<?php if(isset($date_debut)) { echo $date_debut; } ?>"><br/>
                            <label for="date_fin">Date de fin de congé</label>
                            <input type="date" id="date_fin" name="date_fin" required value="<?php if(isset($date_fin)) { echo $date_fin; } ?>"><br/>
                            <label for="nb_jours">Durée</label>
                            <input type="number" id="nb_jours" name="nb_jours" required value="<?php if(isset($nb_jours)) { echo $nb_jours; } ?>"><br/>
                            <input Type="submit" value="Ajouter">
                        </fieldset>
                </fieldset>
            </form>
    </body>
    <footer>
    Adresse : 38 rue Molière 94200 Ivry-sur-Seine <br>
    Télephone : 01 56 20 62 00
    </footer>
</html>          
