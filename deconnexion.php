<?php
    session_start();
    $_SESSION = array();
    session_destroy(); // On détruit la session s'il y en avait une
    unset($_SESSION);
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header('Location: authentification.php');
?>
