-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le :  lun. 09 déc. 2019 à 17:00
-- Version du serveur :  10.4.6-MariaDB
-- Version de PHP :  7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `base_projet`
--

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(30) NOT NULL,
  `mail_admin` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mdp_admin` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `admin`
--

INSERT INTO `admin` (`id_admin`, `mail_admin`, `mdp_admin`) VALUES
(1, 'truc@esme.fr', '1234');

-- --------------------------------------------------------

--
-- Structure de la table `commentaire`
--

CREATE TABLE `commentaire` (
  `id_com` int(100) NOT NULL,
  `com` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `conges`
--

CREATE TABLE `conges` (
  `id_conge` int(40) NOT NULL,
  `id_salarie` int(40) NOT NULL,
  `date_debut` date NOT NULL,
  `date_fin` date NOT NULL,
  `etat` bit(2) NOT NULL DEFAULT b'0',
  `nb_jours` int(2) NOT NULL,
  `comms` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `conges`
--

INSERT INTO `conges` (`id_conge`, `id_salarie`, `date_debut`, `date_fin`, `etat`, `nb_jours`, `comms`) VALUES
(1, 30, '2019-12-11', '2019-12-19', b'00', 2, NULL),
(2, 36, '2019-12-02', '2019-12-25', b'00', 2, NULL),
(3, 30, '2019-12-05', '2019-12-13', b'00', 23, NULL),
(4, 30, '2019-12-05', '2019-12-13', b'00', 23, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `salarie`
--

CREATE TABLE `salarie` (
  `id_salarie` int(40) NOT NULL,
  `nom` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fonction` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_contrat` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_embauche` date NOT NULL,
  `nb_conge_rtt` int(200) NOT NULL,
  `nb_conge_paye` int(200) NOT NULL,
  `mdp_salarie` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adresse` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nationalite` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sexe` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `situation` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` int(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `salarie`
--

INSERT INTO `salarie` (`id_salarie`, `nom`, `prenom`, `mail`, `telephone`, `fonction`, `type_contrat`, `date_embauche`, `nb_conge_rtt`, `nb_conge_paye`, `mdp_salarie`, `adresse`, `nationalite`, `sexe`, `situation`, `age`) VALUES
(1, 'Michel', 'SARDOU', 'mail.test@esme.fr', '06123456', 'Employe', 'CDI', '2019-01-01', 3, 3, 'mdp', 'test', '', '', 'celibataire', 19),
(25, 'pi', 'ant', 'ant.pi@esme.fr', '06777777', 'elmployÃ©', 'CDD', '2017-01-01', 20, 10, 'abc', NULL, NULL, NULL, NULL, NULL),
(27, 'Pujol', 'AurÃ©lien', 'aure.pujol@esme.fr', '06888888', 'esmployÃ©', 'CDD', '2002-10-03', 2, 1, '0Cu9qZm5', NULL, NULL, NULL, NULL, NULL),
(29, 'Goldman', 'Jean', 'je.gold@esme.fr', '06695738', 'employÃ©', 'CDD', '1800-09-09', 40, 3, 'a2eYy5sL', NULL, NULL, NULL, NULL, NULL),
(30, 'Destremau', 'Jean', 'je.des@esme.fr', '06444444', 'employÃ©', 'CDI', '2019-01-01', 3, 4, 'kR1TyMcZ', NULL, NULL, NULL, NULL, NULL),
(31, 'warfe', 'adfr', 'edzad.ed@esme.fr', '06123123', 'enseignant', 'CDD', '2019-06-06', 3, 4, 'k32CrK64', NULL, NULL, NULL, NULL, NULL),
(36, 'dzd', 'dzdz', 'dede.dede@esme.fr', '06333332', 'enseignant', 'CDD', '2019-01-03', 3, 2, '7TqYiH7H', NULL, NULL, NULL, NULL, NULL),
(37, 'dzza', 'dzd', 'dzdz.dzdz@esme.fr', '09888888', 'enseignant', 'CDD', '2018-02-02', 2, 2, 'cT1Us2x7', NULL, NULL, NULL, NULL, NULL),
(38, 'fez', 'fezfz', 'fef.fef@esme.fr', '09999999', 'enseignant', 'CDD', '2000-09-04', 2, 12, '5Ui4v66B', NULL, NULL, NULL, NULL, NULL);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`),
  ADD UNIQUE KEY `id_admin` (`id_admin`,`mdp_admin`),
  ADD UNIQUE KEY `mail_admin` (`mail_admin`);

--
-- Index pour la table `commentaire`
--
ALTER TABLE `commentaire`
  ADD PRIMARY KEY (`id_com`);

--
-- Index pour la table `conges`
--
ALTER TABLE `conges`
  ADD PRIMARY KEY (`id_conge`),
  ADD KEY `id_salarie` (`id_salarie`);

--
-- Index pour la table `salarie`
--
ALTER TABLE `salarie`
  ADD PRIMARY KEY (`id_salarie`),
  ADD UNIQUE KEY `id_salarie` (`id_salarie`),
  ADD UNIQUE KEY `mail` (`mail`,`telephone`),
  ADD UNIQUE KEY `mdp_salarie` (`mdp_salarie`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `commentaire`
--
ALTER TABLE `commentaire`
  MODIFY `id_com` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT pour la table `conges`
--
ALTER TABLE `conges`
  MODIFY `id_conge` int(40) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `salarie`
--
ALTER TABLE `salarie`
  MODIFY `id_salarie` int(40) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `conges`
--
ALTER TABLE `conges`
  ADD CONSTRAINT `conges_ibfk_1` FOREIGN KEY (`id_salarie`) REFERENCES `salarie` (`id_salarie`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
