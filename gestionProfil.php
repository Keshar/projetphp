<?php
    session_start(); // Demarrage de la session
    
    // Permet d'eviter qu'un salarié puisse aller ici
    if(!isset($_SESSION['profil']) || $_SESSION['profil'] != 'salarie'){
        header('Location: index.php');
    }
    
    // Vérification de la duree de la session
    if (!isset($_SESSION['timeout_idle'])) {
        $_SESSION['timeout_idle'] = time() + 2*24*60*60; // Si existe pas on créer pour 48h
    } 
    else {
        if ($_SESSION['timeout_idle'] < time()) {   // Si temps ecoulé => deconnexion
            header('Location: deconnexion.php');
        } 
        else {
            $_SESSION['timeout_idle'] = time() + 2*24*60*60; // Sinon on ralonge
        }
    }
?>


<!DOCTYPE html>
<!--- DESCRIPTION : Ce fichier est divisé en 4 grandes partie :
                        - le menu n'affichant que les liens correspond à "gestion du profil"
                        - l'apercu du profil qui contient un tableau (informations récupéré à partir de l'id stocké dans la session)
                        - la modification des informations du salarie (informations pré-entré à partir de l'id stocké dans la session)
                        - la modification du mot de passe du salarie (verification de l'ancien de passe, du format du nouveau mot de passe 
                                de la non similarité avec un autre mot de passe de la base de donnée) -->


<html>
    <head>
        <meta charset="UTF-8">
        <title>Gestion Des Congés</title>
        <img src="Logo_esme.jpg" height="200" width="320" alt>
    <head>
        <!--- MENU DE NAVIGATION -->
        <nav>
            <a href="gestionConges.php">Accueil</a>
            <a href="gestionProfil.php">Gestion du profil</a>
            <a href="contact.php">Contact</a>
            <a href="deconnexion.php">Déconnexion</a>
            </br></br>
            <a href="gestionProfil.php?redirection=apercu">Apercu du profil</a></br>
            <a href="gestionProfil.php?redirection=modif_info">Modifier ses informations</a></br>
            <a href="gestionProfil.php?redirection=modif_mdp">Modifier son mot de passe</a></br>
            </br>
        </nav>
    
    
       
        <?php  // *** MODIFICATION DES INFORMATIONS DU SALARIE *** 
        if (isset($_GET['redirection']) && $_GET['redirection'] == 'modif_info'){
            // *** CONNEXION A LA BDD ***
            $connect = mysqli_connect("127.0.0.1","root","","base_projet");
            try{
                $bdd = new PDO('mysql:host=localhost;dbname=base_projet;charset=utf8', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
            }
            catch(Exception $e){
                die('Erreur :' .$e->getMessage());
            }

            // *** RECUPERATION DES INFOS ***
            $verif = $bdd->query('SELECT * FROM salarie WHERE id_salarie = ' . $_SESSION['id']);
            while($var = $verif->fetch()){
                $nom = $var['nom'];
                $prenom = $var['prenom'];                    
                $telephone = $var['telephone'];
                $adresse = $var['adresse'];
                $situation = $var['situation'];
                $age = $var['age'];
            }
            $verif->closeCursor();

            $id = $_SESSION['id'];
            
            // *** SET REMPLACEMENT DES INFORMATIONS ***
            if(isset($_POST["telephone"]) && isset($_POST["nom"]) && isset($_POST["prenom"]) && isset($_POST["adresse"]) && isset($_POST["age"]) && isset($_POST["situation"])){
                $telephone = mysqli_real_escape_string($connect,$_POST["telephone"]);
                $adresse = mysqli_real_escape_string($connect, $_POST["adresse"]);
                $situation = mysqli_real_escape_string($connect, $_POST["situation"]);
                $nom = mysqli_real_escape_string($connect, $_POST["nom"]);
                $prenom = mysqli_real_escape_string($connect, $_POST["prenom"]);
                $age = mysqli_real_escape_string($connect, $_POST["age"]);
                
                // *** VERIFICATION DU FORMAT DU NUMERO DE TELEPHONE ***
                if(preg_match("#^[0][0-9]{7}$#", $telephone)) {
                    $sql = "update salarie set telephone='$telephone', nom='$nom', prenom='$prenom', situation='$situation', adresse='$adresse', age='$age' WHERE id_salarie='$id'";
                    if (mysqli_query($connect, $sql)){
                        $message= "Le salarié a été mis à jour avec succes</br>";
                        echo $message;
                    } 
                    else {
                        $message = "Erreur de mise à jour " ;
                        echo $message;
                    }
                }
                else{
                     echo "<p style ='color:red'>Les informations ne sont pas valides </br>(ex: le numéro de téléphone doit contenir 8 charactères)</p>";
                }
            }
                ?>
                <!--- FORMULAIRE PRE-REMPLI POUR LA MODIFICATION DES INFORMATIONS -->
                <form name="exe" action="gestionProfil.php?redirection=modif_info" method="post">
                    <fieldset>
                            <legend>Modifier ses informations</legend><br/>
                            <label for="nom">Nom</label>
                            <input type="text" id="nom" name="nom" required value="<?php if(isset($nom)) { echo $nom; } ?>"><br/>
                            <label for="prenom">Prénom</label>
                            <input type="text" id="prenom" name="prenom" required value="<?php if(isset($prenom)) { echo $prenom; } ?>"><br/>
                            <label for="telephone">Numéro de téléphone</label>
                            <input type="text" id="telephone" name="telephone" required value="<?php if(isset($telephone)) { echo $telephone; } ?>"><br/>
                            <label for="adresse">Adresse</label>
                            <input type="text" id="adresse" name="adresse" value="<?php if(isset($adresse)) { echo $adresse; } ?>"><br/>
                            <label for="situation">Situation</label>
                            <select name="situation" id="situation">
                                <option value="" ><?php if(isset($situation)) { echo $situation; } ?></option>
                                <option value="celibataire" >Célibataire</option>
                                <option value="marie" >Marié.e</option> 
                                <option value="veuf" >Veuf.ve</option> 
                                <option value="paxe" >Paxé.e</option> 
                            </select>   <br/>
                            <label for="age">Age</label>
                            <input type="number" id="age" name="age" value="<?php if(isset($age)) { echo $age; } ?>"><br/><br/>
                            <input Type="submit" value="Modifier">
                    </fieldset>
                </form>
        <?php
        }
        
        
        
        
        // *** MODIFICATION DU MOT DE PASSE ***
        else if (isset($_GET['redirection']) && $_GET['redirection'] == 'modif_mdp'){
            
            // *** CONNEXION A LA BDD ***
            $connect = mysqli_connect("127.0.0.1","root","","base_projet");
            try{
                $bdd = new PDO('mysql:host=localhost;dbname=base_projet;charset=utf8', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
            }
            catch(Exception $e){
                die('Erreur :' .$e->getMessage());
            }

            // *** RECUPERATION DES INFOS ***
            $verif = $bdd->query('SELECT * FROM salarie WHERE id_salarie = ' . $_SESSION['id']);
            while($var = $verif->fetch()){
                $mdp = $var['mdp_salarie'];
            }
            $verif->closeCursor();

            $id = $_SESSION['id'];

            // *** SET DES 3 MOT DE PASSE ENTRES ***
            if(isset($_POST["mdp"]) && isset($id) && isset($_POST["new_mdp1"]) && isset($_POST["new_mdp2"])){   
                if($_POST["new_mdp1"] == $_POST["new_mdp2"]){ // VERIFICATION DE LA SIMILARITEE ENTRE LES DEUX NOUVEAU MDP
                    if($mdp == $_POST["mdp"]){ // ON VERIFIE QUE L'ANCIEN MOT DE PASSE CORRESPOND
                        $mdp = mysqli_real_escape_string($connect, $_POST["mdp"]);
                        $new_mdp1 = $_POST["new_mdp1"];
                        $new_mdp2 = $_POST["new_mdp2"];
                        if(preg_match("#^[A-Z][a-zA-Z0-9]{6,}[0-9]$#", $new_mdp1) && $new_mdp1 != $mdp) { // ON VERIFIE LE FORMAT ET SI LE NOUVEAU MOT DE PASSE EST IDENTIQUE A L'ANCIEN
                            // ON VERIFIE QUE LE MOT DE PASSE EXISTE PAS DANS LA BASE
                            $sql = "update salarie set mdp_salarie='$new_mdp1' WHERE id_salarie='$id'";
                            if (mysqli_query($connect, $sql)){
                                $message= "Votre mot de passe a été mis à jour avec succès!</br>";
                                echo $message;
                            } 
                            else {
                                $message = "<p style ='color:red'>Le mot de passe est déjà utilisé. </p>" ;
                                echo $message;
                            }
                        }
                        else{
                             echo "<p style ='color:red'>Le mot de passe doit contenir : </br> - au moins 8 caractères </br> - commencer par une lettre majuscule </br> - se terminer par un chiffre </br> - être différant de l'ancien mot de passe</p>";
                        }
                    }
                    else{
                        $message = "<p style ='color:red'>Mot de passe actuel erroné, veuillez réessayer.</p>" ;
                        echo $message;
                    }
                }
                else{
                    $message = "<p style ='color:red'>Mots de passe différants, veuillez entrez un nouveau mot de passe similaire.</p>" ;
                    echo $message;
                } 
            }
        ?> <!--- FORMULAIRE DE MODIFICATION DE MOT DE PASSE -->
            <form name="exe" action="gestionProfil.php?redirection=modif_mdp" method="post">
                <fieldset>
                        <legend>Modification du mot de passe</legend><br/>
                        <label for="mdp">Mot de passe actuel </label>
                        <input type="password" name="mdp" id="mdp" required/><br/>
                        <label for="mdp">Nouveau mot de passe </label>
                        <input type="password" name="new_mdp1" id="mdp" required/><br/>
                        <label for="mdp">Confirmez le mot de passe </label>
                        <input type="password" name="new_mdp2" id="mdp" required/><br/><br/>
                        <input Type="submit" value="Modifier">
                </fieldset>
            </form>
        <?php
        }
        
        
        
        
        // *** APERCU DES INFORMATIONS DU SALARIE ***
        else if (isset($_GET['redirection']) && $_GET['redirection'] == 'apercu'){
            // *** CONNEXION A LA BDD ***
            // $connect = mysqli_connect("127.0.0.1","root","","base_projet");
            try{
                $bdd = new PDO('mysql:host=localhost;dbname=base_projet;charset=utf8', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
            }
            catch(Exception $e){
                die('Erreur :' .$e->getMessage());
            }

            // *** RECUPERATION DES INFOS ***
            $verif = $bdd->query('SELECT * FROM salarie WHERE id_salarie = ' . $_SESSION['id']);
            while($var = $verif->fetch()){
                $nom = $var['nom'];
                $prenom = $var['prenom'];          
                $age = $var['age'];
                $situation = $var['situation'];
                $telephone = $var['telephone'];
                $adresse = $var['adresse'];
                $mail = $var['mail'];
                $type_contrat = $var['type_contrat'];
                $fonction = $var['fonction'];
            }
            $verif->closeCursor();

            $id = $_SESSION['id'];    
        ?> <!--- AFFICHAGE DES INFORMATIONS SOUS FORME DE TABLEAU HTML -->
            <table  border=1>
                 <tr>
                     <th width="200"/>Nom</th>
                     <td><?php echo $nom ;?></td>
                 </tr>
                 <tr>
                     <th>Prénom</th>
                     <td><?php echo $prenom ;?></td>
                 </tr>
                 <tr>
                     <th>Age</th>
                     <td><?php echo $age ;?></td>
                 </tr>
                 <tr>
                     <th>Situation familiale</th>
                     <td><?php echo $situation ;?></td>
                 </tr>
                 <tr>
                     <th>Numéro de téléphone</th>
                     <td><?php echo $telephone ;?></td>
                 </tr>
                 <tr>
                     <th>Adresse</th>
                     <td><?php echo $adresse ;?></td>
                 </tr>
                 <tr>
                     <th>Adresse mail</th>
                     <td><?php echo $mail ;?></td>
                 </tr>
                 <tr>
                     <th>Type de contrat</th>
                     <td><?php echo $type_contrat ;?></td>
                 </tr>
                 <tr>
                     <th>Fonction</th>
                     <td><?php echo $fonction ;?></td>
                 </tr>
            </table>
    <br>
    
        <?php
        }
        ?>
    
    <footer><br/>
        Adresse : 38 rue Molière 94200 Ivry-sur-Seine <br>
        Télephone : 01 56 20 62 00
    </footer>
</html>