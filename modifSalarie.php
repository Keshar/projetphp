<?php>
    session_start(); // Demarrage de la session
    
    // Vérification de l'existance d'une session 
    // Permet d'eviter d'aller sur une page avec son url sans sessions
    // Permet d'eviter qu'un salarié puisse aller ici
    if(!isset($_SESSION['profil']) || $_SESSION['profil'] != 'admin'){
        header('Location: index.php');
    }
    
    // Vérification de la duree de la session
    if (!isset($_SESSION['timeout_idle'])) {
        $_SESSION['timeout_idle'] = time() + 2*24*60*60; // Si existe pas on créer pour 48h
    } 
    else {
        if ($_SESSION['timeout_idle'] < time()) {   // Si temps ecoulé => deconnexion
            header('Location: deconnexion.php');
        } 
        else {
            $_SESSION['timeout_idle'] = time() + 2*24*60*60; // Sinon on ralonge
        }
    }
?>

<!DOCTYPE html>

<html>

    <head>
        <meta charset="UTF-8">
        <title>Gestion Des Congés</title>
        <img src="Logo_esme.jpg" height="200" width="270" > <!-- attention j'ai niquée les dimensions-->
    </head>
    <body>
        <nav>
            <a href="gestionConges.php">Accueil</a>
            <a href="gestionSalarie.php">Gestion Salariés</a>
            <a href="deconnxion.php">Déconnexion</a>
	</nav>
        
        <?php
        $connect = mysqli_connect("127.0.01","root","","base_projet");
        if (!$connect) {
            echo"echec de connection";
        }
       
        //Après appel de la page on récupéré l'id en question
        if(isset($_GET["id"]))
        {
	//protection de données
            $id_salarie = mysqli_real_escape_string($connect,$_GET["id"]);
            $sql = "SELECT * FROM salarie WHERE id_salarie=$id_salarie";
            $result = mysqli_query($connect, $sql) or die(mysqli_error($connect));
            if (mysqli_num_rows($result) > 0) {
            // Récupérer des informations en question qui seront par la suite afficher dans le formulaire en bas
                $row = mysqli_fetch_assoc($result);
                
                $telephone=$row["telephone"];
                $adresse=$row["adresse"];
                $situation=$row["situation"];
                $type_contrat=$row["type_contrat"];
                $nom=$row["nom"];
                $prenom=$row["prenom"];
                $age=$row["age"];
            }  
            else{
                    //Si erreur envoie de message à la page ajout_livre.php
            $message="le salarié est introuvable";
            echo $message;
            header("Location:gestionSalaire.php?message=$message");
            }
        }
        // Après clic sur le bouton modifier on récupère les données envoyées par la méthode post
        //if(isset($_POST["telephone"]) /*&& isset($_POST["adresse"]) && isset($_POST["situation"])*/ && isset($_POST["type_contrat"]) && isset($_POST["nom"]) && isset($_POST["prenom"]) /*&& isset($_POST["prenom"])*/ ){
        if(isset($_POST["telephone"]) && isset($_POST["type_contrat"]) && isset($_POST["nom"]) && isset($_POST["prenom"]) )
        {   
            $id_salarie = mysqli_real_escape_string($connect,$_GET["id"]);
            $telephone = mysqli_real_escape_string($connect,$_POST["telephone"]);
            $adresse = mysqli_real_escape_string($connect, $_POST["adresse"]);
            $situation = mysqli_real_escape_string($connect, $_POST["situation"]);
            // $type_contrat = mysqli_real_escape_string($connect, $_POST["type_contrat"]);
            // $nom = mysqli_real_escape_string($connect, $_POST["nom"]);
            // $prenom = mysqli_real_escape_string($connect, $_POST["prenom"]);
            // $age = mysqli_real_escape_string($connect, $_POST["age"]);
            
            if(preg_match("#^[0][0-9]{7}$#", $telephone))
            {
                $sql = "update salarie set telephone='$telephone', type_contrat='$type_contrat', nom='$nom', prenom='$prenom' WHERE id_salarie='$id_salarie'";
                //executer le requete de l'update et redirection vers la page ajoutSalarie.php
                //$var = mysqli_query($connect, $sql) or die(mysqli_error($connect));
                //if ($var) 
                if (mysqli_query($connect, $sql)){
                    $message= "Le salarié a été mis à jour avec succes";
                } 
                else {
                    $message = "Erreur de mise à jour " ;
                }
                header("Location:gestionSalarie.php?message=$message");
            }
            else
                 echo "<p style ='color:red'>Le salarié n'est pas valide </p>";
        }
        ?>
<!--  Afficher le formulaire rempli par les données du livre récupéré en haut.-->
<form name="exe" action="modifSalarie.php?id=<?php echo $_GET["id"];?>" method="post">
      		<fieldset>
      			<legend>Modifier un Salarié</legend>
      			<input type="hidden" id_salarie="id" name="id_salarie" value="<?php if(isset($id_salarie)) { echo $id_salarie; } ?>"><br/>
      			<label for="nom">Nom</label>
                        <input type="text" id="nom" name="nom" required value="<?php if(isset($nom)) { echo $nom; } ?>"><br/>
      			<label for="prenom">Prénom</label>
      			<input type="text" id="prenom" name="prenom" required value="<?php if(isset($prenom)) { echo $prenom; } ?>"><br/>
                        <label for="telephone">Numéro de téléphone</label>
                        <input type="text" id="telephone" name="telephone" required value="<?php if(isset($telephone)) { echo $telephone; } ?>"><br/>
      			<label for="type_contrat">Type de Contrat</label>
      			<input type="text" id="type_contrat" name="type_contrat" required value="<?php if(isset($type_contrat)) { echo $type_contrat; } ?>"><br/>
      			<label for="adresse">Adresse</label>
                        <input type="text" id="adresse" name="adresse" value="<?php if(isset($adresse)) { echo $adresse; } ?>"><br/>
      			<label for="situation">Situation</label>
                        <select name="situation" id="situation">
                            <option value="" >Choisir...</option>
                            <option value="celibataire" >Célibataire</option>
                            <option value="marie" >Marié.e</option> 
                            <option value="veuf" >Veuf.ve</option> 
                            <option value="paxe" >Paxé.e</option> 
                        </select>   <br/>
                        <label for="age">Age</label>
      			<input type="int" id="age" name="age" value="<?php if(isset($age)) { echo $age; } ?>"><br/>
                        <input Type="submit" value="Modifier">
      		</fieldset>
      </form>
    </body>
    <footer>
        Adresse : 38 rue Molière 94200 Ivry-sur-Seine <br>
        Télephone : 01 56 20 62 00
    </footer>
</html>