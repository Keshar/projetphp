<?php
// *** DEMARRAGE DE LA SESSION ***
session_start(); 
$_SESSION['profil'] = $_POST['profil'];
$_SESSION['timeout_idle'] = time() + 2*24*60*60; //48h creation du timeout session

// *** CONNEXION A LA BDD ***
//$connect = mysqli_connect("127.0.0.1","root","","base_projet");
try{
    $bdd = new PDO('mysql:host=localhost;dbname=base_projet;charset=utf8', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
}
catch(Exception $e){
    die('Erreur :' .$e->getMessage());
}

// *** VERIFICATION DES INFORMATIONS ***
if(!empty($_POST)){ // Test envoi du formulaire
    if(!empty($mail = $_POST['mail']) && !empty($_POST['mdp'])){ //test y a t'il quelques chose entré
        // On redirige la recherche des tables en suivant si c'est admin ou salarie 
        if (isset($_POST['profil']) && $_POST['profil'] == 'admin'){ 
            // On vérifie que le Pseudo existe dans la BDD admin et que le mot de passe et la fonction est le bon(ne)
            if (isset($_POST['mail']) AND isset($_POST['mdp']))
            {
                $verif = $bdd->prepare('SELECT COUNT(*) FROM admin WHERE mail_admin = :mail ');
                $verif->bindValue(':mail', $_POST['mail'], PDO::PARAM_STR);
                $verif->execute();
                $donnees = $verif->fetchColumn();
                $verif->closeCursor();
                if($donnees == 1){ // On as trouvé un membre avec ce couple profil, mail
                    $verif = $bdd->prepare('SELECT COUNT(*) FROM admin WHERE mdp_admin = :mdp'); 
                    $verif->bindValue(':mdp', $_POST['mdp'], PDO::PARAM_STR);
                    $verif->execute();
                    $donnees = $verif->fetchColumn();
                    $verif->closeCursor();
                    if($donnees == 1){ // On as trouvé un membre avec ce couple mdp, login
                        // *** RECUPERATION DE L'ID ***
                        $verif = $bdd->query('SELECT id_admin FROM admin WHERE mdp_admin = \'' . $_POST['mdp'] . '\'AND mail_admin = \'' . $_POST['mail'] . '\'');
                        while($id_test = $verif->fetch()){
                            $_SESSION['id']=$id_test['id_admin'];
                        }
                        $verif->closeCursor();
                        // *** REDIRECTION ***
                        header('Location: gestionConges.php');
                    }
                    else{ // Mauvais mot de passe
                        header('Location: authentification.php?e=2');
                    }
                }
                else{ // Personne n'existe dans la table avec ce couple profil, mail
                    header('Location: authentification.php?e=1');
                }
            }
        }
        // Autre cas s'il s'agit d'un salarie
        else if (isset($_POST['profil']) && $_POST['profil'] == 'salarie'){
            //On vérifie que le Pseudo existe dans la BDD admin et que le mot de passe et la fonction est le bon(ne)
            if (isset($_POST['mail']) AND isset($_POST['mdp'])){
                $verif = $bdd->prepare('SELECT COUNT(*) FROM salarie WHERE mail = :mail ');
                // $verif->bindValue(':profil', $_POST['pro/fil'], PDO::PARAM_STR);
                $verif->bindValue(':mail', $_POST['mail'], PDO::PARAM_STR);
                $verif->execute();
                $donnees = $verif->fetchColumn();
                $verif->closeCursor();
                if($donnees == 1){ // On as trouvé un membre avec ce couple profil, mail
                    $verif = $bdd->prepare('SELECT COUNT(*) FROM salarie WHERE mdp_salarie = :mdp');                       
                    $verif->bindValue(':mdp', $_POST['mdp'], PDO::PARAM_STR);
                    $verif->execute();
                    $donnees = $verif->fetchColumn();
                    $verif->closeCursor();
                    if($donnees == 1){ // On as trouvé un membre avec ce couple mdp, login
                        // *** RECUPERATION DE L'ID ***
                        $verif = $bdd->query('SELECT id_salarie FROM salarie WHERE mdp_salarie = \'' . $_POST['mdp'] . '\'AND mail = \'' . $_POST['mail'] . '\'');
                        while($id_test = $verif->fetch()){
                            $_SESSION['id']=$id_test['id_salarie'];
                        }
                        $verif->closeCursor();
                        // *** REDIRECTION ***
                        header('Location: gestionConges.php');
                    }
                    else{ // Mauvais mot de passe
                        header('Location: authentification.php?e=2');
                    }
                }
                else{ // Personne n'existe dans la table avec ce couple profil, mail
                    header('Location: authentification.php?e=1');
                }
            }
        }
    }
}
include('assets/header.html');
?>
